package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class GetClubHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT * FROM club_user INNER JOIN club ON club_user.club_id=club.club_id WHERE club_user.email=\"" + params.getUtfString("email") + "\"";
        
//        if(params.getUtfString("search").compareTo("name") == 0) {
//        	sql += " WHERE club_name LIKE \"%" + params.getUtfString("clubname") + "%\"";
//			try {
//	        	Integer result = Integer.parseInt(params.getUtfString("id"));
//	        	sql += " AND club_id=" + result;
//			} catch (NumberFormatException ex) {
//			}
//        }
//        System.out.println(sql);

        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
                         
            // Populate the response parameters
            ISFSObject response = new SFSObject();
            response.putSFSArray("array", res);
              
            // Send back to requester
            send("getClub", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
