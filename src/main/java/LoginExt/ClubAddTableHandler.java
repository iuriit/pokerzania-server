package LoginExt;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.CreateRoomSettings.RoomExtensionSettings;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class ClubAddTableHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
        int i = 0;
        System.out.println(System.currentTimeMillis());
        int curTime = (int)(System.currentTimeMillis() / 1000);
        System.out.println(curTime);
        String roomName = params.getUtfString("tableName") + "_" + params.getInt("club_id") + "_" + curTime;

		CreateRoomSettings settings = new CreateRoomSettings();
		settings.setName(roomName);
		settings.setGroupId("Club");
		settings.setGame(true);
		settings.setExtension(new RoomExtensionSettings("PokerExtension", "org.dsaw.poker.PokerExtension"));
		settings.setAutoRemoveMode(SFSRoomRemoveMode.NEVER_REMOVE);
		
		ISFSObject resObj = new SFSObject();
		resObj.putInt("duration", params.getInt("duration"));
		resObj.putInt("club_id", params.getInt("club_id"));
		resObj.putInt("startTime", (int)curTime);
	
		List<RoomVariable> roomVars = new ArrayList<RoomVariable>(); 
		roomVars.add(new SFSRoomVariable("tableSize", params.getInt("tableSize")));
		roomVars.add(new SFSRoomVariable("isFast", true));
		roomVars.add(new SFSRoomVariable("buyin", params.getInt("buyin")));
		roomVars.add(new SFSRoomVariable("blind", params.getInt("blind")));
		roomVars.add(new SFSRoomVariable("clubValue", resObj));
		settings.setRoomVariables(roomVars);
		
		try {
			getApi().createRoom(getParentExtension().getParentZone(), settings, user, false, getParentExtension().getParentZone().getRoomByName("The Lobby"));
		} catch (SFSCreateRoomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();

        String sql = "INSERT INTO club_table(room_name, table_name, start_time, buyin, blind, table_size, club_id) VALUES("
        		+ "\"" + roomName + "\","
        		+ "\"" + params.getUtfString("tableName") + "\","
        		+ (int)curTime + ","
        		+ params.getInt("buyin") + ","
        		+ params.getInt("blind") + ","
        		+ params.getInt("tableSize") + ","
        		+ params.getInt("club_id") + ")";
          
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
            send("clubAddTable", new SFSObject(), user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
	
}
