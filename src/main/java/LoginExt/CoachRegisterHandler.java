package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class CoachRegisterHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();

        String sql = "SELECT * FROM coach WHERE id=" + params.getInt("id");

        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            if(res.size() > 0)
            	sql = "UPDATE coach"
                		+ " SET fullname='" + params.getUtfString("fullname")
                		+ "',language='" + params.getUtfString("language")
                		+ "',specialization='" + params.getUtfString("specialization")
                		+ "',stakes='" + params.getUtfString("stakes")
                		+ "',levelofplay='" + params.getUtfString("levelofplay")
                		+ "',biodata='" + params.getUtfString("biodata")
                		+ "' WHERE id=" + params.getInt("id");
            else
            	sql = "INSERT INTO coach(id, fullname, language, specialization, stakes, levelofplay, biodata)"
            			+ " VALUES("
            			+ params.getInt("id") + ", '"
            			+ params.getUtfString("fullname") + "','"
            			+ params.getUtfString("language") + "','"
            			+ params.getUtfString("specialization") + "','"
            			+ params.getUtfString("stakes") + "','"
            			+ params.getUtfString("levelofplay") + "','"
            			+ params.getUtfString("biodata") + "')";
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
            send("coachRegister", new SFSObject(), user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
