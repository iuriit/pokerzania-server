package LoginExt;

import com.smartfoxserver.v2.components.signup.SignUpAssistantComponent;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class SignupExtension extends SFSExtension {
	
	private SignUpAssistantComponent suac;
	
	@Override
	public void init() {
		suac = new SignUpAssistantComponent();
		
		suac.getConfig().signUpTable = "users";
		suac.getConfig().emailField = "email";
		suac.getConfig().usernameField = "username";
		suac.getConfig().passwordField = "password";
		suac.getConfig().checkForDuplicateUserNames = false;
		suac.getConfig().minUserNameLength = 1;
		suac.getConfig().minPasswordLength = 0;

		addRequestHandler(SignUpAssistantComponent.COMMAND_PREFIX, suac);
		addRequestHandler("createCash", CreateCashHandler.class);
	}
	
	@Override
	public void destroy() {
		super.destroy();
	}

}
