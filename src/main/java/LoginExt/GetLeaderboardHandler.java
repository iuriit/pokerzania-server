package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class GetLeaderboardHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
		String sql = "SELECT * FROM users INNER JOIN cash ON users.id=cash.id";
		if(params.getInt("searchkey") == 0)
			sql += " ORDER BY chipcount DESC LIMIT 10";
		else if(params.getInt("searchkey") == 1)
			sql += " ORDER BY hand DESC LIMIT 10";
		else if(params.getInt("searchkey") == 2)
			sql += " ORDER BY hour DESC LIMIT 10";
        
        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
              
            // Populate the response parameters
            ISFSObject response = new SFSObject();
            response.putSFSArray("array", res);
              
            // Send back to requester
            send("getLeaderboard", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
