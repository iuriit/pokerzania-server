package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class CashHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
		String sql = "";
		if(params.containsKey("email"))
			sql = "SELECT cash.chipcount FROM cash INNER JOIN users ON users.id=cash.id WHERE users.email='" + params.getUtfString("email") +  "'";
		else
			sql = "SELECT cash.chipcount FROM cash INNER JOIN users ON users.id=cash.id WHERE users.email='" + user.getName() +  "'";
        
        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
              
            // Populate the response parameters
            ISFSObject response = new SFSObject();
            response.putSFSArray("cashArray", res);
              
            // Send back to requester
            send("cash", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
