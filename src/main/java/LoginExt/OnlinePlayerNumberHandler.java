package LoginExt;

import java.util.List;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class OnlinePlayerNumberHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		ISFSObject obj = new SFSObject();
		obj.putInt("number", getParentExtension().getParentZone().getUserCount());
		List<User> list = (List<User>)getParentExtension().getParentZone().getUserList();
		send("onlinePlayerNumber", obj, list);
	}
}
