package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class GetCoachHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT * FROM coach INNER JOIN users ON users.id=coach.id";
    	sql += " WHERE coach.fullname LIKE '%" + params.getUtfString("name") + "%'";
    	if(params.getInt("searchkey") == 0)
    		sql += " ORDER BY coach.rate DESC";
    	else if(params.getInt("searchkey") == 1)
    		sql += " ORDER BY coach.hour DESC";
    	else if(params.getInt("searchkey") == 2)
    		sql += " ORDER BY coach.gross DESC";

        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
                         
            // Populate the response parameters
            ISFSObject response = new SFSObject();
            response.putSFSArray("coachArray", res);
              
            // Send back to requester
            send("getCoach", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
