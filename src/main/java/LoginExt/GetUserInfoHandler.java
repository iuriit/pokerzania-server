package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class GetUserInfoHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		String email = params.getUtfString("email");
		int handPlay = handsPlay(email);
		
		System.out.println(handPlay);
		
		ISFSObject response = new SFSObject();
		response.putInt("handsPlay", handPlay);
		response.putInt("biggestWin", biggestWin(email));
		response.putInt("vpip", vpip(email, handPlay));
		response.putInt("pfr", pfr(email, handPlay));
		response.putFloat("af", af(email));
        send("getUserInfo", response, user);
	}
	
	public int handsPlay(String _email)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT COUNT(hand_id) FROM hand_history WHERE"
        			+ " email=\"" + _email + "\""
        			+ " AND type=0";
        try {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});              
            return res.getSFSObject(0).getLong("COUNT(hand_id)").intValue();
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return 0;
	}

	public int biggestWin(String _email)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT COUNT(hand_id) FROM hand_history WHERE type=0 AND email=\"" + _email + "\"";

        try {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});              
            if(res.getSFSObject(0).getLong("COUNT(hand_id)") == 0)
            	return 0;
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		
		sql = "SELECT MAX(amount) FROM hand_history WHERE type=0 AND email=\"" + _email + "\"";
        try {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});              
            return res.getSFSObject(0).getInt("MAX(amount)");
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return 0;
	}

	public int vpip(String _email, int handPlay)
	{
		if(handPlay == 0)
			return 0;
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT COUNT(hand_id) FROM hand_history WHERE email=\"" + _email + "\" AND type=1 AND action='Fold'";
        try {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});              
            int fold_count = res.getSFSObject(0).getLong("COUNT(hand_id)").intValue();
            return fold_count * 100 / handPlay;
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return 0;
	}

	public int pfr(String _email, int handPlay)
	{
		if(handPlay == 0)
			return 0;
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT COUNT(hand_id) FROM hand_history WHERE email=\"" + _email + "\" AND type=1 AND action='Raise'";
        try {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});              
            int fold_count = res.getSFSObject(0).getLong("COUNT(hand_id)").intValue();
            return fold_count * 100 / handPlay;
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return 0;
	}

	public float af(String _email)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT COUNT(hand_id) FROM hand_history WHERE email=\"" + _email + "\" AND type=1 AND action='Raise'";
        try {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            int betRaiseCount = getActionCount(_email, "Bet") + getActionCount(_email, "Raise");
            int callCount = getActionCount(_email, "Call");
            if(callCount == 0)
            {
            	if(betRaiseCount == 0)
            		return 0;
            	else
            		return 99999;
            }
            return (float)betRaiseCount / callCount;
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return 0;
	}
	
	public int getActionCount(String _email, String _action)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT COUNT(hand_id) FROM hand_history WHERE email=\"" + _email + "\" AND action='" + _action + "'";
        try {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});              
            return res.getSFSObject(0).getLong("COUNT(hand_id)").intValue();
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return 0;
	}

}
