package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class ClubTradeHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		payClubChip(params.getInt("club_id"), params.getUtfString("fromUser"), params.getInt("amount"));
		payClubChip(params.getInt("club_id"), params.getUtfString("toUser"), 0 - params.getInt("amount"));
	}
	
	public void payClubChip(int club_id, String username, int amount)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT * FROM club_user WHERE"
        			+ " email=\"" + username + "\" AND"
        			+ " club_id=" + club_id;
        
        int chip = 0;
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            chip = res.getSFSObject(0).getInt("club_chip");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }

        sql = "UPDATE club_user SET club_chip=" + (chip - amount) + " WHERE"
    			+ " email=\"" + username + "\" AND"
    			+ " club_id=" + club_id;
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
