package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class GetClubUserHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT * FROM club_user WHERE"
        			+ " email=\"" + params.getUtfString("email") + "\" AND"
        			+ " club_id=" + params.getInt("club_id");
        
        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            ISFSObject response = new SFSObject();
            response.putBool("isExist", res.size() > 0);
            if(res.size() > 0)
            	response.putSFSObject("value", res.getSFSObject(0));
                                       
            // Send back to requester
            send("getClubUser", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
