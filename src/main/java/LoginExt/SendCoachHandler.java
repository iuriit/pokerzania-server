package LoginExt;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class SendCoachHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		String receiver = params.getUtfString("receiver");
		User rcvUser = getParentExtension().getParentZone().getUserByName(receiver);
		if(rcvUser != null)
			send("sendCoach", params, getParentExtension().getParentZone().getUserByName(receiver));
	}
}
