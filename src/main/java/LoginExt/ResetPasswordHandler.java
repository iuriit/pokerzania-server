package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class ResetPasswordHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();

        String sql = "SELECT * FROM users WHERE email=\"" + params.getUtfString("email") + "\"";
        ISFSObject obj = new SFSObject();

        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            if(res.size() > 0)
            {
            	obj.putBool("success", true);
            	sql = "UPDATE users SET password='' WHERE email='" + params.getUtfString("email") + "'";
                try
                {
                    dbManager.executeUpdate(sql, new Object[] {});
                }
                catch (SQLException e)
                {
                    trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
                }
            }
            else
            {
            	obj.putBool("success", false);
            }
            send("resetPassword", obj, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }        
	}
}
