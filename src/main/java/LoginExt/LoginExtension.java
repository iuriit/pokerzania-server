package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.components.login.LoginAssistantComponent;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class LoginExtension extends SFSExtension {
	
	private LoginAssistantComponent lac;
	
	@Override
	public void init() {
		lac = new LoginAssistantComponent(this);
		
		lac.getConfig().loginTable = "users";
		lac.getConfig().userNameField = "email";
		lac.getConfig().passwordField = "password";
		
		addRequestHandler("resetPassword", ResetPasswordHandler.class);
		addRequestHandler("user", UserHandler.class);
		addRequestHandler("setAvatar", SetAvatarHandler.class);
		addRequestHandler("cash", CashHandler.class);
		addRequestHandler("getCoach", GetCoachHandler.class);
		addRequestHandler("hireCoach", HireCoachHandler.class);
		addRequestHandler("rateCoach", RateCoachHandler.class);
		addRequestHandler("sendCoach", SendCoachHandler.class);
		addRequestHandler("coachRegister", CoachRegisterHandler.class);
		addRequestHandler("createRoom", CreateRoomHandler.class);
		addRequestHandler("locationInfo", LocationInfoHandler.class);
		addRequestHandler("onlinePlayerNumber", OnlinePlayerNumberHandler.class);
		addRequestHandler("getClub", GetClubHandler.class);
		addRequestHandler("getClubUser", GetClubUserHandler.class);
		addRequestHandler("getClubMember", GetClubMemberHandler.class);
		addRequestHandler("createClub", CreateClubHandler.class);
		addRequestHandler("joinClub", JoinClubHandler.class);
		addRequestHandler("clubTrade", ClubTradeHandler.class);
		addRequestHandler("clubAddTable", ClubAddTableHandler.class);
		addRequestHandler("getClubTable", GetClubTableHandler.class);
		addRequestHandler("getLeaderboard", GetLeaderboardHandler.class);
		addRequestHandler("getLevel", GetLevelHandler.class);
		addRequestHandler("getUserData", GetUserDataHandler.class);
		addRequestHandler("getUserInfo", GetUserInfoHandler.class);
	}
	
	@Override
	public void destroy() {
		super.destroy();
	}
	
	public String getUserName(String email)
	{
		String name = "";
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "SELECT username FROM users WHERE email='" + email +  "'";
        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            name = res.getSFSObject(0).getUtfString("username");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return name;
	}

}
