package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class CreateCashHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT id FROM users WHERE email='" + params.getUtfString("email") +  "'";
        int id = -1;
        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            ISFSObject obj = (ISFSObject)(res.getElementAt(0));
            id = obj.getInt("id");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }

        sql = "INSERT INTO cash(id, chipcount) VALUES("
						+ id + "," + 10000 + ")";
          
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
            send("createCash", new SFSObject(), user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
