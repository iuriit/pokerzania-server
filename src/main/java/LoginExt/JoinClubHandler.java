package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class JoinClubHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();

		ISFSObject response = new SFSObject();
		ISFSArray array = getClub(params);
		if(array == null || array.size() == 0)
		{
			response.putBool("success", false);
	        send("joinClub", response, user);
	        return;
		}
		
		response.putBool("success", true);
        send("joinClub", response, user);

        String sql = "SELECT * FROM club_user WHERE email=\"" + params.getUtfString("email") + "\"";
		Integer result = 0;
		try {
	    	result = Integer.parseInt(params.getUtfString("club_id"));
	    	sql += " AND club_id=" + result;
		} catch (NumberFormatException ex) {
		}

		try
        {
	        ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
	        if(res.size() > 0)
	        	return;
	    }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		
    	sql = "INSERT INTO club_user(email, club_id, club_chip)"
    			+ " VALUES(\""
    			+ params.getUtfString("email") + "\","
    			+ result + ","
    			+ 0 + ")";
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
	
	ISFSArray getClub(ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();

    	String sql = "SELECT * FROM club WHERE"
    			+ " club_name LIKE \"%" + params.getUtfString("clubname") + "%\"";
		try {
	    	Integer result = Integer.parseInt(params.getUtfString("club_id"));
	    	sql += " AND club_id=" + result;
		} catch (NumberFormatException ex) {
		}

		try
        {
	        ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
	        return res;
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return null;
	}
	
}
