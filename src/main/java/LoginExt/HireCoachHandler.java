package LoginExt;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class HireCoachHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		String sender = params.getUtfString("sender");
		String receiver = params.getUtfString("receiver");
		User rcvUser = getParentExtension().getParentZone().getUserByName(receiver);
		System.out.println(sender + "->" + receiver + " : " + params.getInt("action"));
		if(rcvUser != null)
		{
			System.out.println("ok");
			send("hireCoach", params, getParentExtension().getParentZone().getUserByName(receiver));
		}
		else
		{
			System.out.println("fail");
			rcvUser = getParentExtension().getParentZone().getUserByName(sender);
			if(rcvUser != null)
			{
				params.putInt("action", -1);
				send("hireCoach", params, getParentExtension().getParentZone().getUserByName(sender));
			}
		}
	}
}
