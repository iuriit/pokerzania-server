package LoginExt;

import java.sql.SQLException;

import org.dsaw.poker.engine.PlayMode;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class RateCoachHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		insertHistory(params);
		float rate = getRate(params.getUtfString("coachEmail"));
		float hour = getHour(params.getInt("id")) + params.getFloat("hour");
		int gross = getGross(params.getInt("id")) + params.getInt("gross");
		updateCoach(params.getInt("id"), rate, hour, gross);
		setPlayerChipcount(params.getUtfString("email"), getPlayerChipcount(params.getUtfString("email")) - params.getInt("gross"));
		setPlayerChipcount(params.getUtfString("coachEmail"), getPlayerChipcount(params.getUtfString("coachEmail")) + params.getInt("gross"));
	}
	
	public void insertHistory(ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "INSERT INTO coach_history(id, email, rate, hour, gross) VALUES("
        				+ params.getInt("id") + ",'"
           				+ params.getUtfString("coachEmail") + "',"
        				+ params.getFloat("rate") + ","
        				+ params.getFloat("hour") + ","
        				+ params.getInt("gross") + ")";
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
	
	public float getRate(String email)
	{
		float rate = 0f;
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT rate FROM coach_history WHERE email='" + email +  "'";        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            for(int i = 0; i < res.size(); i ++) {
            	ISFSObject obj = res.getSFSObject(i);
            	rate += obj.getFloat("rate");
            }
            rate /= res.size();
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return rate;
	}

	public float getHour(int id)
	{
		float hour = 0f;
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT hour FROM coach WHERE id=" + id;
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            hour = res.getSFSObject(0).getFloat("hour");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return hour;
	}

	public int getGross(int id)
	{
		int gross = 0;
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT gross FROM coach WHERE id=" + id;
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            gross = res.getSFSObject(0).getInt("gross");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		return gross;
	}

	public void updateCoach(int id, float rate, float hour, int gross)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
		String sql = "UPDATE coach SET rate=" + rate + " AND hour=" + hour + " AND gross=" + gross + " WHERE id=" + id;
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
    
    public void setPlayerChipcount(String email, int value)
    {
    	IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "UPDATE cash INNER JOIN users ON cash.id=users.id SET cash.chipcount=" + value + " WHERE users.email='" + email + "'";
        
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
    }
    
    public int getPlayerChipcount(String email)
    {
        int chipcount = 0;
    	IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
    	String sql = "SELECT cash.chipcount FROM cash INNER JOIN users ON users.id=cash.id WHERE users.email='" + email + "'";
        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            chipcount = res.getSFSObject(0).getInt("chipcount");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return chipcount;
    }

}
