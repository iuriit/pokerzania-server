package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class CreateClubHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();

    	String sql = "INSERT INTO club(owner_email, club_name, description, club_chip)"
    			+ " VALUES(\""
    			+ params.getUtfString("email") + "\",\""
    			+ params.getUtfString("club_name") + "\",\""
    			+ params.getUtfString("description") + "\","
    			+ params.getInt("club_chip") + ")";
        
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }

        int club_id = -1;
        sql = "SELECT * FROM club WHERE"
        		+ " owner_email=\"" + params.getUtfString("email") + "\" AND"
        		+ " club_name=\"" + params.getUtfString("club_name") + "\" AND"
        		+ " description=\"" + params.getUtfString("description") + "\"";
        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});                         
            club_id = res.getSFSObject(0).getInt("club_id");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }

    	sql = "INSERT INTO club_user(email, club_id, club_chip)"
    			+ " VALUES(\""
    			+ params.getUtfString("email") + "\","
    			+ club_id + ","
    			+ params.getInt("club_chip") + ")";        
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        
        payCash(user.getName(), params.getInt("club_chip"));

        send("createClub", new SFSObject(), user);
	}
	
	private void payCash(String username, int amount)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        
		String sql = "SELECT cash.chipcount FROM cash INNER JOIN users ON users.id=cash.id WHERE users.email='" + username +  "'";
        int chip = 0;
        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});              
            chip = res.getSFSObject(0).getInt("chipcount");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        
        sql = "UPDATE cash INNER JOIN users ON cash.id=users.id SET cash.chipcount=" + (chip - amount) + " WHERE users.email='" + username + "'";        
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
