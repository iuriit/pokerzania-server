package LoginExt;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class UserHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
    	IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT * FROM users WHERE email='" + user.getName() +  "'";
        ISFSObject resObj = new SFSObject();
        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            resObj.putSFSArray("userArray", res);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
		send("user", resObj, user);
	}
}
