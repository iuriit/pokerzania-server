package LoginExt;

import java.util.ArrayList;
import java.util.List;

import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.CreateRoomSettings.RoomExtensionSettings;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class CreateRoomHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
        int i = 0;
        String roomName = params.getUtfString("roomName");
        while(true)
        {
        	if(getParentExtension().getParentZone().getRoomByName(roomName) == null)
        		break;
        	roomName = params.getUtfString("roomName") + " (" + (++i) + ")";
        }

		CreateRoomSettings settings = new CreateRoomSettings();
		settings.setName(roomName);
		settings.setGroupId(params.getUtfString("groupId"));
		settings.setGame(params.getBool("isGame"));
		settings.setExtension(new RoomExtensionSettings(params.getUtfString("extensionId"), params.getUtfString("extensionClass")));
		settings.setAutoRemoveMode(SFSRoomRemoveMode.NEVER_REMOVE);
		
		List<RoomVariable> roomVars = new ArrayList<RoomVariable>(); 
		roomVars.add(new SFSRoomVariable("tableSize", params.getInt("tableSize")));
		roomVars.add(new SFSRoomVariable("isFast", params.getBool("isFast")));
		roomVars.add(new SFSRoomVariable("buyin", params.getInt("buyin")));
		roomVars.add(new SFSRoomVariable("blind", params.getInt("blind")));
		settings.setRoomVariables(roomVars);
		
		try {
			getApi().createRoom(getParentExtension().getParentZone(), settings, user, true, getParentExtension().getParentZone().getRoomByName("The Lobby"));
		} catch (SFSCreateRoomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
