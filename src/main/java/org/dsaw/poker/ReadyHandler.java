package org.dsaw.poker;

import org.dsaw.poker.engine.Player;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ReadyHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		System.out.println("Ready");
		PokerExtension gameExt = (PokerExtension) getParentExtension();

		if (user.isPlayer())
		{
			String userName = user.getName();
			for(Player player : gameExt.table.players) {
				System.out.println(player.getEmail() + ", " + user.getName());
				if(userName.compareTo(player.getEmail()) == 0)
				{
			        System.out.println("join1");
			        send("join", new SFSObject(), user);
				}
			}
			for(Player player : gameExt.table.newPlayers) {
				System.out.println(player.getEmail() + ", " + user.getName());
				if(userName.compareTo(player.getEmail()) == 0)
				{
			        System.out.println("join1");
			        send("join", new SFSObject(), user);
				}
			}
	        System.out.println("ready");
			gameExt.updateAll();
			gameExt.sendChipStatus(user.getName());
		}
		
	}
}
