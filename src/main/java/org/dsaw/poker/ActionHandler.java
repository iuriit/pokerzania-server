package org.dsaw.poker;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ActionHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		System.out.println("Action");
		PokerExtension gameExt = (PokerExtension) getParentExtension();

		if (user.isPlayer())
		{
			gameExt.setSelectedAction(params.getInt("action"), params.getInt("value"));
		}
		
	}
}
