package org.dsaw.poker.engine.actions;

public class BustedAction extends Action {

    /**
     * Constructor.
     */
    /* package */ BustedAction() {
        super("Busted", "player busted");
    }
    
}
