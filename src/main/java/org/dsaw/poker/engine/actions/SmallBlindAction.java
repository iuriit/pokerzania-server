package org.dsaw.poker.engine.actions;

public class SmallBlindAction extends Action {

    /**
     * Constructor.
     */
    /* package */ SmallBlindAction() {
        super("Small blind", "posts the small blind");
    }
    
}
