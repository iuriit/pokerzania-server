package org.dsaw.poker.engine.actions;

public class BigBlindAction extends Action {
    
    /**
     * Constructor.
     */
    /* package */ BigBlindAction() {
        super("Big blind", "posts the big blind");
    }
    
}
