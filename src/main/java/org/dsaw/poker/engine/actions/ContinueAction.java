package org.dsaw.poker.engine.actions;

public class ContinueAction extends Action {

    /**
     * Constructor.
     */
    /* package */ ContinueAction() {
        super("Continue", "continues");
    }
    
}
