package org.dsaw.poker.engine.bots;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.dsaw.poker.engine.Card;
import org.dsaw.poker.engine.Player;
import org.dsaw.poker.engine.TableType;
import org.dsaw.poker.engine.actions.Action;
import org.dsaw.poker.engine.actions.BetAction;
import org.dsaw.poker.engine.actions.RaiseAction;
import org.dsaw.poker.engine.util.PokerUtils;

public class BasicBot extends Bot {
    
    /** Tightness (0 = loose, 100 = tight). */
    private final int tightness;
    
    /** Betting aggression (0 = safe, 100 = aggressive). */
    private final int aggression;
    
    /** Table type. */
    private TableType tableType;
    
    /** The hole cards. */
    private Card[] cards;

    private static int BOT_WAITING_TIME = 1;
    private Timer timer;
    private boolean delayFlag;
    private final Object monitor = new Object();

    /**
     * Constructor.
     * 
     * @param tightness
     *            The bot's tightness (0 = loose, 100 = tight).
     * @param aggression
     *            The bot's aggressiveness in betting (0 = careful, 100 =
     *            aggressive).
     */
    public BasicBot(int tightness, int aggression) {
        if (tightness < 0 || tightness > 100) {
            throw new IllegalArgumentException("Invalid tightness setting");
        }
        if (aggression < 0 || aggression > 100) {
            throw new IllegalArgumentException("Invalid aggression setting");
        }
        this.tightness = tightness;
        this.aggression = aggression;
    }

    /** {@inheritDoc} */
    @Override
    public void joinedTable(Player playerToNotify, TableType type, BigDecimal bigBlind, List<Player> players) {
        this.tableType = type;
    }

    /** {@inheritDoc} */
    @Override
    public void messageReceived(Player playerToNotify, String message) {
        // Not implemented.
    }

    /** {@inheritDoc} */
    @Override
    public void handStarted(Player playerToNotify, Player dealer) {
        cards = null;
    }

    @Override
    public void setBlind(Player playerToNotify, Player blind, String blindText)
    {
    }

    /** {@inheritDoc} */
    @Override
    public void actorRotated(Player playerToNotify, Player actor) {
        // Not implemented.
    }

    @Override
    public void selectActor(Player playerToNofity, Player actor, boolean show)
    {
    }

    /** {@inheritDoc} */
    @Override
    public void boardUpdated(Player playerToNotify, List<Card> cards, BigDecimal bet, BigDecimal pot) {
        // Not implemented.
    }

    /** {@inheritDoc} */
    @Override
    public void playerUpdated(Player playerToNotify, Player player) {
        if (player.getCards().length == NO_OF_HOLE_CARDS) {
            this.cards = player.getCards();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void playerActed(Player playerToNotify, Player player) {
        // Not implemented.
    }

    /** {@inheritDoc} */
    @Override
    public Action act(Player playerToNotify, BigDecimal minBet, BigDecimal currentBet, Set<Action> allowedActions) {
        Action action;
        if (allowedActions.size() == 1) {
            // No choice, must check.
            action = Action.CHECK;
        } else {
            double chenScore = PokerUtils.getChenScore(cards);
            double chenScoreToPlay = tightness * 0.2;
            if ((chenScore < chenScoreToPlay)) {
                if (allowedActions.contains(Action.CHECK)) {
                    // Always check for free if possible.
                    action = Action.CHECK;
                } else {
                    // Bad hole cards; play tight.
                    action = Action.FOLD;
                }
            } else {
                // Good enough hole cards, play hand.
                if ((chenScore - chenScoreToPlay) >= ((20.0 - chenScoreToPlay) / 2.0)) {
                    // Very good hole cards; bet or raise!
                    if (aggression == 0) {
                        // Never bet.
                        if (allowedActions.contains(Action.CALL)) {
                            action = Action.CALL;
                        } else {
                            action = Action.CHECK;
                        }
                    } else if (aggression == 100) {
                        // Always go all-in!
                        //FIXME: Check and bet/raise player's remaining cash.
                        BigDecimal amount = (tableType == TableType.FIXED_LIMIT) ? minBet : minBet.multiply(BigDecimal.TEN.multiply(BigDecimal.TEN));
                        if (allowedActions.contains(Action.BET)) {
                            action = new BetAction(amount);
                        } else if (allowedActions.contains(Action.RAISE)) {
                            action = new RaiseAction(amount);
                        } else if (allowedActions.contains(Action.CALL)) {
                            action = Action.CALL;
                        } else {
                            action = Action.CHECK;
                        }
                    } else {
                        BigDecimal amount = minBet;
                        if (tableType == TableType.NO_LIMIT) {
                            int betLevel = aggression / 20;
                            for (int i = 0; i < betLevel; i++) {
                                amount = amount.add(amount);
                            }
                        }
                        if (currentBet.compareTo(amount) < 0) {
                            if (allowedActions.contains(Action.BET)) {
                                action = new BetAction(amount);
                            } else if (allowedActions.contains(Action.RAISE)) {
                                action = new RaiseAction(amount);
                            } else if (allowedActions.contains(Action.CALL)) {
                                action = Action.CALL;
                            } else {
                                action = Action.CHECK;
                            }
                        } else {
                            if (allowedActions.contains(Action.CALL)) {
                                action = Action.CALL;
                            } else {
                                action = Action.CHECK;
                            }
                        }
                    }
                } else {
                    // Decent hole cards; check or call.
                    if (allowedActions.contains(Action.CHECK)) {
                        action = Action.CHECK;
                    } else {
                        action = Action.CALL;
                    }
                }
            }
        }
        delayTimer();
        return action;
    }

    private void delayTimer() {
    	delayFlag = true;
		// SetTimer
    	timer = new Timer();
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
			    // Your database code here
				  if(delayFlag) {
					  delayFlag = false;
					  synchronized (monitor) {
						  monitor.notifyAll();
					  }
				  }
			  }
			}, BOT_WAITING_TIME * 1000);
		
		while(delayFlag) {
            // Wait for the user to select an action.
            synchronized (monitor) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    // Ignore.
                }
            }
		}
        timer.cancel();
    }

}
