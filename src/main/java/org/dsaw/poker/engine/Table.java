package org.dsaw.poker.engine;

import org.dsaw.poker.engine.actions.Action;
import org.dsaw.poker.engine.actions.BetAction;
import org.dsaw.poker.engine.actions.RaiseAction;

import org.dsaw.poker.PokerExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.Timer;
import java.util.TreeMap;

public class Table {
    
    /** In fixed-limit games, the maximum number of raises per betting round. */
    private static final int MAX_RAISES = 3;
    
    /** Whether players will always call the showdown, or fold when no chance. */
    private static final boolean ALWAYS_CALL_SHOWDOWN = false;

    public final PlayMode playMode;
    
    /** Table type (poker variant). */
    private final TableType tableType;
    
    /** The size of the big blind. */
    private final BigDecimal bigBlind;
    
    private final PokerExtension gameExt;

    /** The players at the table. */
    public List<Player> players;
    public List<Player> newPlayers;
    public List<Player> exitPlayers;
    public List<Player> handPlayers;
    public boolean isRunning;
    
    /** The active players in the current hand. */
    private final List<Player> activePlayers;
    
    /** The deck of cards. */
    private final Deck deck;
    
    /** The community cards on the board. */
    public final List<Card> board;
    
    /** The current dealer position. */
    private int dealerPosition;

    /** The current dealer. */
    private Player dealer;

    /** The position of the acting player. */
    private int actorPosition;
    
    /** The acting player. */
    public Player actor;

    /** The minimum bet in the current hand. */
    private BigDecimal minBet;

    /** The current bet in the current hand. */
    public BigDecimal bet;

    /** All pots in the current hand (main pot and any side pots). */
    private final List<Pot> pots;
    
    /** The player who bet or raised last (aggressor). */
    private Player lastBettor;
    
    /** Number of raises in the current betting round. */
    private int raises;

    public int club_id;
    public int duration;
    public int startTime;

    private Timer timer;
    private boolean delayFlag;
    private final Object monitor = new Object();
    
    public int dealerPos;
    public int smallBlindPos;
    public int bigBlindPos;
    public boolean isSelect = false;
    public int[] tournamentPayouts = {200, 150, 100};
    
    /**
     * Constructor.
     * 
     * @param bigBlind
     *            The size of the big blind.
     */
    public Table(PlayMode mode, TableType type, BigDecimal bigBlind, PokerExtension _client) {
    	this.playMode = mode;
        this.tableType = type;
        this.bigBlind = bigBlind;
        this.gameExt = _client;
        players = new ArrayList<>();
        newPlayers = new ArrayList<>();
        activePlayers = new ArrayList<>();
        exitPlayers = new ArrayList<>();
        handPlayers = new ArrayList<>();
        deck = new Deck();
        board = new ArrayList<>();
        pots = new ArrayList<>();
        timer = new Timer();
        this.isRunning = false;
    }
    
    /**
     * Adds a player.
     * 
     * @param player
     *            The player.
     */
    public void addPlayer(Player player) {
        players.add(player);
    }
    
    public boolean isRunning() {
    	return isRunning;
    }
    
    public int playerSize() {
    	return players.size() + newPlayers.size();
    }
    
    /**
     * Main game loop.
     */
    public void run() {
    	int handStartTime, handEndTime;
    	isRunning = true;
        dealerPosition = -1;
        actorPosition = -1;
        while (true) {
        	
        	for (Player player : newPlayers) {
        		players.add(player);
        	}
        	newPlayers.clear();

        	for (Player player : exitPlayers) {
    			players.remove(player);
    			gameExt.removePlayer(player);
        	}
        	exitPlayers.clear();

        	Collections.sort(players);
        	gameExt.updatePlayers(false);

        	if(playMode == PlayMode.CLUB_MODE) {
        		int curTime = (int)(System.currentTimeMillis() / 1000);
				int diff = curTime - startTime;
        		if(diff > duration * 3600) {
        			for(Player player : players) {
        				int chip = gameExt.getPlayerChipcount(player.getEmail());
        				gameExt.setPlayerChipcount(player.getEmail(), chip + player.getCash().intValue());
        			}
        	        gameExt.kickPlayers();
        			return;
        		}
        	}

            int noOfActivePlayers = 0;
            for (Player player : players) {
                if (player.getCash().compareTo(bigBlind) >= 0) {
                    noOfActivePlayers++;
                    handPlayers.add(player);
                }
            }
            handStartTime = (int)(System.currentTimeMillis() / 1000);
            if (noOfActivePlayers > 1) {
                playHand();
            } else {
                break;
            }
            handEndTime = (int)(System.currentTimeMillis() / 1000);
            int diff = handEndTime - handStartTime;
            
            for (Player player : handPlayers) {
            	if(player.isBot())
            		continue;
            	gameExt.addHour(player.getEmail(), (float)diff / 3600);
            }
            handPlayers.clear();

            for (Player player : activePlayers) {
            	if (player.getCash().compareTo(bigBlind) < 0) {
            		player.setAction(Action.BUSTED);
                    notifyMessage(player, "Busted");
                }
        	}
            doPayout();
        }
        
        // Game over.
        board.clear();
        pots.clear();
        bet = BigDecimal.ZERO;
        for (Player player : activePlayers) {
        	if (player.getCash().compareTo(bigBlind) > 0 && !player.isBot()) {
        		int chipcount = gameExt.getPlayerChipcount(player.getName());
        		if(this.playMode == PlayMode.TOURNAMENT_MODE)
            		gameExt.setPlayerChipcount(player.getEmail(), chipcount + tournamentPayouts[0]);
        		else
        			gameExt.setPlayerChipcount(player.getEmail(), chipcount + player.getCash().intValue());
        		gameExt.sendChipStatus(player.getEmail());
        		notifyMessage(player, "Congratulations!\nYou have won the game");
        	}
        }
        notifyBoardUpdated();
        for (Player player : players) {
            player.resetHand();
        }
        notifyPlayersUpdated(false);
//        notifyMessage("Game over.");
        for(int i = 0; i < 10; i ++)
        	delayTimer();
        gameExt.kickPlayers();
    }
    
    private void doPayout()
    {
		System.out.println(this.playMode.getName());
    	if(this.playMode != PlayMode.TOURNAMENT_MODE)
    		return;
    	
    	int bustedNum = 0, amount = 0;
        for (Player player : activePlayers) {
        	if (player.getCash().compareTo(bigBlind) < 0) {
        		bustedNum ++;
            }
    	}
        
        if(bustedNum == 0 || activePlayers.size() - bustedNum >= 3)
        	return;
        for(int i = activePlayers.size() - bustedNum; i < 3; i ++)
        	amount += tournamentPayouts[i];
        amount /= bustedNum;

        System.out.println("BustedNum = " + bustedNum + ", Amount = " + amount);

        for (Player player : activePlayers) {
        	if (player.getCash().compareTo(bigBlind) < 0 && !player.isBot()) {
        		System.out.println(player.getName() + " = " + amount);
        		int chipcount = gameExt.getPlayerChipcount(player.getEmail());
        		gameExt.setPlayerChipcount(player.getEmail(), chipcount + amount);
            }
    	}
    }
    
    /**
     * Plays a single hand.
     */
    private void playHand() {
        resetHand();
        
        // Small blind.
        if (activePlayers.size() > 2) {
            rotateActor();
        }
        postSmallBlind();
        
        // Big blind.
        rotateActor();
        postBigBlind();
        
        // Pre-Flop.
        delayTimer();
        dealHoleCards();
        delayTimer();
        doBettingRound(1);
        
        // Flop.
        if (activePlayers.size() > 1) {
            bet = BigDecimal.ZERO;
            delayTimer();
            dealCommunityCards("Flop", 3);
            delayTimer();
            doBettingRound(2);

            // Turn.
            if (activePlayers.size() > 1) {
                bet = BigDecimal.ZERO;
                delayTimer();
                dealCommunityCards("Turn", 1);
                delayTimer();
                minBet = bigBlind.add(bigBlind);
                doBettingRound(3);

                // River.
                if (activePlayers.size() > 1) {
                    bet = BigDecimal.ZERO;
                    delayTimer();
                    dealCommunityCards("River", 1);
                    delayTimer();
                    doBettingRound(4);

                    // Showdown.
                    if (activePlayers.size() > 1) {
                        bet = BigDecimal.ZERO;
                        delayTimer();
                        doShowdown();
                        delayTimer();
                    }
                }
            }
        }
    }
    
    /**
     * Resets the game for a new hand.
     */
    private void resetHand() {
        // Clear the board.
        board.clear();
        pots.clear();
        notifyBoardUpdated();
        
        // Determine the active players.
        activePlayers.clear();
        for (Player player : players) {
            player.resetHand();
            // Player must be able to afford at least the big blind.
            if (player.getCash().compareTo(bigBlind) >= 0) {
                activePlayers.add(player);
            }
        }
        
        // Rotate the dealer button.
        dealerPosition = (dealerPosition + 1) % activePlayers.size();
        dealer = activePlayers.get(dealerPosition);

        // Shuffle the deck.
        deck.shuffle();

        // Determine the first player to act.
        actorPosition = dealerPosition;
        actor = activePlayers.get(actorPosition);
        
        // Set the initial bet to the big blind.
        minBet = bigBlind;
        bet = minBet;
        
        // Notify all clients a new hand has started.
        gameExt.setBlind(dealer, "(D)");
        dealerPos = dealer.getPos();
        notifyPlayersUpdated(false);
        notifyMessage("New hand, %s is the dealer.", dealer);
    }

    /**
     * Rotates the position of the player in turn (the actor).
     */
    private void rotateActor() {
        actorPosition = (actorPosition + 1) % activePlayers.size();
        actor = activePlayers.get(actorPosition);
//        for (Player player : players) {
//            player.getClient().actorRotated(player, actor);
//        }
    }
    
    private void selectActor(boolean show)
    {
//        for (Player player : players) {
//            player.getClient().selectActor(player, actor, show);
//        }
    	isSelect = show;
        gameExt.selectActor(actor, show);
    }
    
    /**
     * Posts the small blind.
     */
    private void postSmallBlind() {
        final BigDecimal smallBlind = bigBlind.divide(BigDecimal.valueOf(2)); //TODO
        actor.postSmallBlind(smallBlind);
        contributePot(smallBlind);
        notifyBoardUpdated();
        smallBlindPos = actor.getPos();
        gameExt.setBlind(actor, "(SB)");
//        for (Player player : players) {
//            player.getClient().setBlind(player, actor, "(SB)");
//        }
//        notifyPlayerActed();
    }
    
    /**
     * Posts the big blind.
     */
    private void postBigBlind() {
        actor.postBigBlind(bigBlind);
        contributePot(bigBlind);
        notifyBoardUpdated();
        bigBlindPos = actor.getPos();
        gameExt.setBlind(actor, "(BB)");
///        notifyPlayerActed();
    }
    
    /**
     * Deals the Hole Cards.
     */
    private void dealHoleCards() {
        for (Player player : activePlayers) {
            player.setCards(deck.deal(2));
        }
        notifyPlayersUpdated(false);
        delayTimer();
        notifyMessage("%s deals the hole cards.", dealer);
    }
    
    /**
     * Deals a number of community cards.
     * 
     * @param phaseName
     *            The name of the phase.
     * @param noOfCards
     *            The number of cards to deal.
     */
    private void dealCommunityCards(String phaseName, int noOfCards) {
        for (int i = 0; i < noOfCards; i++) {
            board.add(deck.deal());
        }
        notifyPlayersUpdated(false);
        delayTimer();
        notifyMessage("%s deals the %s.", dealer, phaseName);
    }
    
    /**
     * Performs a betting round.
     */
    private void doBettingRound(int round) {
        // Determine the number of active players.
        int playersToAct = activePlayers.size();
        // Determine the initial player and bet size.
        if (board.size() == 0) {
            // Pre-Flop; player left of big blind starts, bet is the big blind.
            bet = bigBlind;
        } else {
            // Otherwise, player left of dealer starts, no initial bet.
            actorPosition = dealerPosition;
            bet = BigDecimal.ZERO;
        }
        
        if (playersToAct == 2) {
            // Heads Up mode; player who is not the dealer starts.
            actorPosition = dealerPosition;
        }
        
        lastBettor = null;
        raises = 0;
        notifyBoardUpdated();
                
        while (playersToAct > 0) {
            rotateActor();
            Action action;
            if (actor.isAllIn()) {
                // Player is all-in, so must check.
                action = Action.CHECK;
                playersToAct--;
            } else {
                // Otherwise allow client to act.
                Set<Action> allowedActions = getAllowedActions(actor);
                selectActor(true);
                action = actor.getClient().act(actor, minBet, bet, allowedActions);
                if(action == null)
                {
                	action = Action.FOLD;
                	delayTimer();
                }
                selectActor(false);
                // Verify chosen action to guard against broken clients (accidental or on purpose).
                if (!allowedActions.contains(action)) {
                    if (action instanceof BetAction && !allowedActions.contains(Action.BET)) {
                        throw new IllegalStateException(String.format("Player '%s' acted with illegal Bet action", actor));
                    } else if (action instanceof RaiseAction && !allowedActions.contains(Action.RAISE)) {
                        throw new IllegalStateException(String.format("Player '%s' acted with illegal Raise action", actor));
                    }
                }
                playersToAct--;
                if (action == Action.CHECK) {
                    if(!actor.isBot())
                    	gameExt.addAction(actor.getEmail(), round, "Check", 0);
                    // Do nothing.
                } else if (action == Action.CALL) {
                    BigDecimal betIncrement = bet.subtract(actor.getBet());
                    if (betIncrement.compareTo(actor.getCash()) > 0) {
                        betIncrement = actor.getCash();
                    }
                    actor.payCash(betIncrement);
                    actor.setBet(actor.getBet().add(betIncrement));
                    if(!actor.isBot())
                    	gameExt.addAction(actor.getEmail(), round, "Call", betIncrement.intValue());
                    contributePot(betIncrement);
                } else if (action instanceof BetAction) {
                    BigDecimal amount = (tableType == TableType.FIXED_LIMIT) ? minBet : action.getAmount();
                    if (amount.compareTo(minBet) < 0 && amount.compareTo(actor.getCash()) < 0) {
                        throw new IllegalStateException("Illegal client action: bet less than minimum bet!");
                    }
                    if(!actor.isBot())
                    	gameExt.addAction(actor.getEmail(), round, "Bet", amount.intValue());
                    actor.setBet(amount);
                    actor.payCash(amount);
                    contributePot(amount);
                    bet = amount;
                    minBet = amount;
                    lastBettor = actor;
//                    playersToAct = activePlayers.size();
                    playersToAct = activePlayers.size() - 1;
                } else if (action instanceof RaiseAction) {
                    BigDecimal amount = (tableType == TableType.FIXED_LIMIT) ? minBet : action.getAmount();
                    if (amount.compareTo(minBet) < 0 && amount.compareTo(actor.getCash()) < 0) {
                        throw new IllegalStateException("Illegal client action: raise less than minimum bet!");
                    }
                    bet = bet.add(amount);
                    minBet = amount;
                    BigDecimal betIncrement = bet.subtract(actor.getBet());
                    if (betIncrement.compareTo(actor.getCash()) > 0) {
                        betIncrement = actor.getCash();
                    }
                    actor.setBet(bet);
                    actor.payCash(betIncrement);
                    contributePot(betIncrement);
                    if(!actor.isBot())
                    	gameExt.addAction(actor.getEmail(), round, "Raise", betIncrement.intValue());
                    lastBettor = actor;
                    raises++;
//                    if (tableType == TableType.NO_LIMIT || raises < MAX_RAISES || activePlayers.size() == 2) { 
//                        // All players get another turn.
//                        playersToAct = activePlayers.size();
//                    } else {
//                        // Max. number of raises reached; other players get one more turn.
//                        playersToAct = activePlayers.size() - 1;
//                    }
                    playersToAct = activePlayers.size() - 1;
                } else if (action == Action.FOLD) {
                    actor.setCards(null);
                    activePlayers.remove(actor);
                    actorPosition--;
                    if(!actor.isBot()) {
                    	gameExt.addAction(actor.getEmail(), round, "Fold", 0);
                    	gameExt.addAction(actor.getEmail(), 0, "", 0);
                    	gameExt.addHand(actor.getEmail());
                    	actor.isDone = true;
                    }
                    if (activePlayers.size() == 1) {
                        // Only one player left, so he wins the entire pot.
                        notifyBoardUpdated();
                        notifyPlayerActed();
                        Player winner = activePlayers.get(0);
                        BigDecimal amount = getTotalPot();
                        winner.win(amount);
                        if(!winner.isBot())
                        {
                        	gameExt.addAction(winner.getEmail(), 0, "", amount.intValue());
                        	gameExt.addHand(winner.getEmail());
                        	winner.isDone = true;
                        }
                        notifyBoardUpdated();
                        notifyMessage("%s wins $ %d.", winner, amount.intValue());
                        playersToAct = 0;
                    }
                } else {
                    // Programming error, should never happen.
                    throw new IllegalStateException("Invalid action: " + action);
                }
            }
            if(actor.getCash().equals(BigDecimal.ZERO) && (action != Action.FOLD))
            	action = Action.ALL_IN;
            actor.setAction(action);
            System.out.println(action.getName());
//            if (playersToAct > 0) {
                notifyBoardUpdated();
                notifyPlayersUpdated(false);
//                notifyPlayerActed();
//            }
        }
        
        delayTimer();
        // Reset player's bets.
        for (Player player : activePlayers) {
            player.resetBet();
        }
        notifyBoardUpdated();
        notifyPlayersUpdated(false);
    }
    
    /**
     * Returns the allowed actions of a specific player.
     * 
     * @param player
     *            The player.
     * 
     * @return The allowed actions.
     */
    private Set<Action> getAllowedActions(Player player) {
        Set<Action> actions = new HashSet<>();
        if (player.isAllIn()) {
            actions.add(Action.CHECK);
        } else {
            BigDecimal actorBet = actor.getBet();
            if (bet.equals(BigDecimal.ZERO)) {
                actions.add(Action.CHECK);
                if (tableType == TableType.NO_LIMIT || raises < MAX_RAISES || activePlayers.size() == 2) {
                    actions.add(Action.BET);
                }
            } else {
                if (actorBet.compareTo(bet) < 0) {
                    actions.add(Action.CALL);
                    if (tableType == TableType.NO_LIMIT || raises < MAX_RAISES || activePlayers.size() == 2) {
                        actions.add(Action.RAISE);
                    }
                } else {
                    actions.add(Action.CHECK);
                    if (tableType == TableType.NO_LIMIT || raises < MAX_RAISES || activePlayers.size() == 2) {
                        actions.add(Action.RAISE);
                    }
                }
            }
            actions.add(Action.FOLD);
        }
        return actions;
    }
    
    /**
     * Contributes to the pot.
     * 
     * @param amount
     *            The amount to contribute.
     */
    private void contributePot(BigDecimal amount) {
        for (Pot pot : pots) {
            if (!pot.hasContributer(actor)) {
                BigDecimal potBet = pot.getBet();
                if (amount.compareTo(potBet) >= 0) {
                    // Regular call, bet or raise.
                    pot.addContributer(actor);
                    amount = amount.subtract(pot.getBet());
                } else {
                    // Partial call (all-in); redistribute pots.
                    pots.add(pot.split(actor, amount));
                    amount = BigDecimal.ZERO;
                }
            }
            if (amount.compareTo(BigDecimal.ZERO) <= 0) {
                break;
            }
        }
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            Pot pot = new Pot(amount);
            pot.addContributer(actor);
            pots.add(pot);
        }
    }
    
    /**
     * Performs the showdown.
     */
    private void doShowdown() {
//        System.out.println("\n[DEBUG] Pots:");
//        for (Pot pot : pots) {
//            System.out.format("  %s\n", pot);
//        }
//        System.out.format("[DEBUG]  Total: %d\n", getTotalPot());
        
        // Determine show order; start with all-in players...
        List<Player> showingPlayers = new ArrayList<>();
        for (Pot pot : pots) {
            for (Player contributor : pot.getContributors()) {
                if (!showingPlayers.contains(contributor) && contributor.isAllIn()) {
                    showingPlayers.add(contributor);
                }
            }
        }
        // ...then last player to bet or raise (aggressor)...
        if (lastBettor != null) {
            if (!showingPlayers.contains(lastBettor)) {
                showingPlayers.add(lastBettor);
            }
        }
        //...and finally the remaining players, starting left of the button.
        int pos = (dealerPosition + 1) % activePlayers.size();
        while (showingPlayers.size() < activePlayers.size()) {
            Player player = activePlayers.get(pos);
            if (!showingPlayers.contains(player)) {
                showingPlayers.add(player);
            }
            pos = (pos + 1) % activePlayers.size();
        }
        
        // Players automatically show or fold in order.
        boolean firstToShow = true;
        int bestHandValue = -1;
        for (Player playerToShow : showingPlayers) {
            Hand hand = new Hand(board);
            hand.addCards(playerToShow.getCards());
            HandValue handValue = new HandValue(hand);
            boolean doShow = ALWAYS_CALL_SHOWDOWN;
            if (!doShow) {
                if (playerToShow.isAllIn()) {
                    // All-in players must always show.
                    doShow = true;
                    firstToShow = false;
                } else if (firstToShow) {
                    // First player must always show.
                    doShow = true;
                    bestHandValue = handValue.getValue();
                    firstToShow = false;
                } else {
                    // Remaining players only show when having a chance to win.
//                    if (handValue.getValue() >= bestHandValue) {
                        doShow = true;
                        bestHandValue = handValue.getValue();
//                    }
                }
            }
            if (doShow) {
                // Show hand.
//                for (Player player : players) {
//                    player.getClient().playerUpdated(player, playerToShow);
//                }
            	notifyPlayersUpdated(true);
                notifyMessage("%s has %s.", playerToShow, handValue.getDescription());
            } else {
                // Fold.
                playerToShow.setCards(null);
                activePlayers.remove(playerToShow);
                if(!playerToShow.isBot()) {
                	playerToShow.isDone = true;
                	gameExt.addAction(playerToShow.getEmail(), 0, "", 0);
                	gameExt.addAction(playerToShow.getEmail(), 4, "Fold", 0);
                	gameExt.addHand(playerToShow.getEmail());
                }
//                for (Player player : players) {
//                    if (player.equals(playerToShow)) {
//                        player.getClient().playerUpdated(player, playerToShow);
//                    } else {
//                        // Hide secret information to other players.
//                        player.getClient().playerUpdated(player, playerToShow.publicClone());
//                    }
//                }
            	notifyPlayersUpdated(false);
                notifyMessage("%s folds.", playerToShow);
            }
        }
        delayTimer();

        // Sort players by hand value (highest to lowest).
        Map<HandValue, List<Player>> rankedPlayers = new TreeMap<>();
        for (Player player : activePlayers) {
            // Create a hand with the community cards and the player's hole cards.
            Hand hand = new Hand(board);
            hand.addCards(player.getCards());
            // Store the player together with other players with the same hand value.
            HandValue handValue = new HandValue(hand);
//            System.out.format("[DEBUG] %s: %s\n", player, handValue);
            List<Player> playerList = rankedPlayers.get(handValue);
            if (playerList == null) {
                playerList = new ArrayList<>();
            }
            playerList.add(player);
            rankedPlayers.put(handValue, playerList);
        }

        // Per rank (single or multiple winners), calculate pot distribution.
        BigDecimal totalPot = getTotalPot();
        Map<Player, BigDecimal> potDivision = new HashMap<>();
        for (HandValue handValue : rankedPlayers.keySet()) {
            List<Player> winners = rankedPlayers.get(handValue);
            for (Pot pot : pots) {
                // Determine how many winners share this pot.
                int noOfWinnersInPot = 0;
                for (Player winner : winners) {
                    if (pot.hasContributer(winner)) {
                        noOfWinnersInPot++;
                    }
                }
                if (noOfWinnersInPot > 0) {
                    // Divide pot over winners.
                    BigDecimal potShare = pot.getValue().divide(new BigDecimal(String.valueOf(noOfWinnersInPot))); //TODO
                    for (Player winner : winners) {
                        if (pot.hasContributer(winner)) {
                            BigDecimal oldShare = potDivision.get(winner);
                            if (oldShare != null) {
                                potDivision.put(winner, oldShare.add(potShare));
                            } else {
                                potDivision.put(winner, potShare);
                            }
                            
                        }
                    }
                    // Determine if we have any odd chips left in the pot.
                    BigDecimal oddChips = pot.getValue().remainder(new BigDecimal(String.valueOf(noOfWinnersInPot))); //TODO
                    if (oddChips.compareTo(BigDecimal.ZERO) > 0) {
                        // Divide odd chips over winners, starting left of the dealer.
                        pos = dealerPosition;
                        while (oddChips.compareTo(BigDecimal.ZERO) > 0) {
                            pos = (pos + 1) % activePlayers.size();
                            Player winner = activePlayers.get(pos);
                            BigDecimal oldShare = potDivision.get(winner);
                            if (oldShare != null) {
                                potDivision.put(winner, oldShare.add(BigDecimal.ONE));
//                                System.out.format("[DEBUG] %s receives an odd chip from the pot.\n", winner);
                                oddChips = oddChips.subtract(BigDecimal.ONE);
                            }
                        }
                        
                    }
                    pot.clear();
                }
            }
        }
        
        // Divide winnings.
        StringBuilder winnerText = new StringBuilder();
        BigDecimal totalWon = BigDecimal.ZERO;
        for (Player winner : potDivision.keySet()) {
            BigDecimal potShare = potDivision.get(winner);
            winner.win(potShare);
            if(!winner.isBot()) {
            	gameExt.addAction(winner.getEmail(), 0, "", potShare.intValue());
            	gameExt.addHand(winner.getEmail());
            	winner.isDone = true;
            }
            totalWon = totalWon.add(potShare);
            if (winnerText.length() > 0) {
                winnerText.append(", ");
            }
            winnerText.append(String.format("%s wins $ %d", winner, potShare.intValue()));
            notifyPlayersUpdated(true);
        }
        for (Player player : activePlayers) {
        	if(!player.isDone) {
        		if(!player.isBot()) {
                	gameExt.addAction(player.getEmail(), 0, "", 0);
                	gameExt.addHand(player.getEmail());
                	player.isDone = true;
        		}
        	}
        }
        	
        winnerText.append('.');
        delayTimer();
        notifyMessage(winnerText.toString());
        
//        // Sanity check.
//        if (!totalWon.equals(totalPot)) {
//            throw new IllegalStateException("Incorrect pot division!");
//        }
    }
    
    /**
     * Notifies listeners with a custom game message.
     * 
     * @param message
     *            The formatted message.
     * @param args
     *            Any arguments.
     */
    private void notifyMessage(String message, Object... args) {
//        message = String.format(message, args);
//        for (Player player : players) {
//            player.getClient().messageReceived(player, message);
//        }
    }
    
    private void notifyMessage(Player playerToNotify, String message)
    {
    	playerToNotify.getClient().messageReceived(playerToNotify, message);
    }
    
    /**
     * Notifies clients that the board has been updated.
     */
    private void notifyBoardUpdated() {
//        BigDecimal pot = getTotalPot();
//        for (Player player : players) {
//            player.getClient().boardUpdated(player, board, bet, pot);
//        }
        gameExt.updateBoard();
    }
    
    /**
     * Returns the total pot size.
     * 
     * @return The total pot size.
     */
    public BigDecimal getTotalPot() {
        BigDecimal totalPot = BigDecimal.ZERO;
        for (Pot pot : pots) {
            totalPot = totalPot.add(pot.getValue());
        }
        return totalPot;
    }

    /**
     * Notifies clients that one or more players have been updated. <br />
     * <br />
     * 
     * A player's secret information is only sent its own client; other clients
     * see only a player's public information.
     * 
     * @param showdown
     *            Whether we are at the showdown phase.
     */
    private void notifyPlayersUpdated(boolean showdown) {
//        for (Player playerToNotify : players) {
//            for (Player player : players) {
//                if (!showdown && !player.equals(playerToNotify)) {
//                    // Hide secret information to other players.
//                    player = player.publicClone();
//                }
//                playerToNotify.getClient().playerUpdated(playerToNotify, player);
//            }
//        }
        for (Player playerToNotify : players) {
            for (Player player : players) {
            	if(playerToNotify.isBot()) {
                    if (!showdown && !player.equals(playerToNotify)) {
                        // Hide secret information to other players.
                        player = player.publicClone();
                    }
                    playerToNotify.getClient().playerUpdated(playerToNotify, player);
            	}
            }
        }
        gameExt.updatePlayers(showdown);
    }
    
    /**
     * Notifies clients that a player has acted.
     */
    private void notifyPlayerActed() {
//        for (Player p : players) {
//            Player playerInfo = p.equals(actor) ? actor : actor.publicClone();
//            p.getClient().playerActed(p, playerInfo);
//        }
    	delayTimer();
	}
    
    private void delayTimer() {
    	delayFlag = true;
		// SetTimer
    	timer = new Timer();
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
			    // Your database code here
				  if(delayFlag) {
					  delayFlag = false;
					  synchronized (monitor) {
						  monitor.notifyAll();
					  }
				  }
			  }
			}, 1*1000);
		
		while(delayFlag) {
            // Wait for the user to select an action.
            synchronized (monitor) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    // Ignore.
                }
            }
		}
        timer.cancel();
    }
    
}
