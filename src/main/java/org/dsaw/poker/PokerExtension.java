package org.dsaw.poker;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import org.dsaw.poker.engine.Card;
import org.dsaw.poker.engine.Client;
import org.dsaw.poker.engine.PlayMode;
import org.dsaw.poker.engine.Player;
import org.dsaw.poker.engine.Table;
import org.dsaw.poker.engine.TableType;
import org.dsaw.poker.engine.actions.Action;
import org.dsaw.poker.engine.actions.BetAction;
import org.dsaw.poker.engine.actions.RaiseAction;
import org.dsaw.poker.engine.bots.BasicBot;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class PokerExtension extends SFSExtension implements Client {

    /* The table. */
    public Table table;
	
    /** Serial version UID. */
    private static final long serialVersionUID = -5414633931666096443L;
    
    /** Table type (betting structure). */
    private static final TableType TABLE_TYPE = TableType.NO_LIMIT;

    /** The size of the big blind. */
    private static final BigDecimal BIG_BLIND = BigDecimal.valueOf(10);

    /** The starting cash per player. */
    private static final BigDecimal STARTING_CASH = BigDecimal.valueOf(500);
    
    private static int BOT_WAITING_TIME = 1;
    private static int USER_WAITING_TIME = 20;
    
    private static int CARD_BACK_NUM = 52;
    private static int CARD_PLACEHOLDER_NUM = 53;
    
    /** The human player. */
    private Player humanPlayer;
    
    /** The current dealer's name. */
    private String dealerName; 
    private int dealerPos; 

    /** The current actor's name. */
    private String actorName;
    private int actorPos; 
    
    private Timer timer;
    private TimerTask task;
    private Action selectedAction;
    private final Object monitor = new Object();
    private boolean prevShow = false;
    
    private int tableSize = 8;
    private boolean speed = false;
    private BigDecimal buy_in = BigDecimal.valueOf(500);
    private BigDecimal big_blind = BigDecimal.valueOf(10);

    @Override
    public void init() {
		trace("Poker game Extension for SFS2X started.");
		
		tableSize = this.getGameRoom().getVariable("tableSize").getIntValue();
		speed = this.getGameRoom().getVariable("isFast").getBoolValue();
		int buyin = this.getGameRoom().getVariable("buyin").getIntValue();
		int blind = this.getGameRoom().getVariable("blind").getIntValue();
		buy_in = BigDecimal.valueOf(buyin);
		big_blind = BigDecimal.valueOf(blind);
		
		System.out.println("tableSize:" + tableSize + ",isFast:" + speed);

        /* The table. */
		if(this.getGameRoom().getGroupId().compareTo("Tournament") == 0) {
			System.out.println("Tournament Mode:" + this.getGameRoom().getGroupId());
	        table = new Table(PlayMode.TOURNAMENT_MODE, TABLE_TYPE, big_blind, this);
		}
		else if(this.getGameRoom().getGroupId().compareTo("Club") == 0) {
			System.out.println("Tournament Mode:" + this.getGameRoom().getGroupId());
	        table = new Table(PlayMode.CLUB_MODE, TABLE_TYPE, big_blind, this);
	        ISFSObject obj = this.getGameRoom().getVariable("clubValue").getSFSObjectValue();
	        table.duration = obj.getInt("duration");
	        table.club_id = obj.getInt("club_id");
	        table.startTime = obj.getInt("startTime");
		}
		else {
//			tableSize = 6;
//			speed = true;
//			buy_in = STARTING_CASH;
//			big_blind = BIG_BLIND;

			System.out.println("Normal Mode:" + this.getGameRoom().getGroupId());
			table = new Table(PlayMode.NORMAL_MODE, TABLE_TYPE, big_blind, this);			
		}
        table.newPlayers.add(new Player(1, "", "Joe", 9, buy_in, true, new BasicBot(0, 75)));
        table.newPlayers.add(new Player(3, "", "Mike", 10, buy_in, true, new BasicBot(25, 50)));
        table.newPlayers.add(new Player(4, "", "Cole", 11, buy_in, true, new BasicBot(25, 50)));

        System.out.println("Poker Extension Init");
	    addRequestHandler("ready", ReadyHandler.class);
	    addRequestHandler("join", JoinHandler.class);
	    addRequestHandler("action", ActionHandler.class);
	    addRequestHandler("exitRoom", ExitRoomHandler.class);
    }
    
    public void updateAll()
    {
    	updatePlayers(prevShow);
    	updateBoard();
    	if(table.isRunning())
    	{
    		setBlind(table.dealerPos, "(D)");
    		setBlind(table.smallBlindPos, "(SB)");
    		setBlind(table.bigBlindPos, "(BB)");
    		selectActor(table.actor, table.isSelect);
    	}
    }

    public void sendChipStatus(String _email)
    {
    	if(getParentRoom().getUserByName(_email) == null)
    		return;
    	int amount = getPlayerChipcount(_email);
    	ISFSObject obj = new SFSObject();
    	obj.putInt("amount", amount);
    	send("chipStatus", obj, getParentRoom().getUserByName(_email));
    }
    
    void joinNewPlayer(int _pos, String email)
    {
    	if(table.playerSize() == tableSize)
    		return;
    	int chipcount = getPlayerChipcount(email);
    	if(chipcount < buy_in.intValue())
    	{
    		sendMessage(email, "You don't have enough chips.");
    		return;
    	}
    	setPlayerChipcount(email, chipcount - buy_in.intValue());

    	ISFSObject obj = getUser(email);
    	table.newPlayers.add(new Player(_pos, email, obj.getUtfString("username"), obj.getInt("avatar"), buy_in, false, this));
        send("join", new SFSObject(), getParentRoom().getUserByName(email));
    	updateAll();
    	if(!table.isRunning())
    	{
    		if(table.playMode == PlayMode.NORMAL_MODE) {
    			if(table.playerSize() >= 2)
    				table.run();
    		}
    		else if(table.playMode == PlayMode.TOURNAMENT_MODE) {
    			if(table.playerSize() == tableSize)
    				table.run();
    		}
    		else if(table.playMode == PlayMode.CLUB_MODE) {
    			if(table.playerSize() >= 2)
    				table.run();
    		}
    	}
    }
	
	public void exitPlayer(String email)
	{		
		for(Player player : table.players) {
			if(email.compareTo(player.getEmail()) == 0)
			{
				if(!table.isRunning() || player.getAction() == Action.BUSTED) {
					table.players.remove(player);
					removePlayer(player);
				}
				else {
					table.exitPlayers.add(player);
					player.isExit = true;
				}
				return;
			}
		}
		for(Player player : table.newPlayers) {
			if(email.compareTo(player.getEmail()) == 0)
			{
				table.newPlayers.remove(player);
				removePlayer(player);
				return;
			}
		}
    	if(getParentRoom().getUserByName(email) != null)
        {
        	ISFSObject obj = new SFSObject();
        	obj.putUtfString("email", email);
        	send("kick", obj, getParentRoom().getUserByName(email));
        }
	}
	
	public void removePlayer(Player player)
	{
		int chipcount = getPlayerChipcount(player.getEmail());
		setPlayerChipcount(player.getEmail(), chipcount + player.getCash().intValue());
    	if(getParentRoom().getUserList() != null)
        {
        	ISFSObject obj = new SFSObject();
        	obj.putUtfString("email", player.getEmail());
        	obj.putInt("pos", player.getPos());
        	send("kick", obj, getParentRoom().getUserList());
        }
	}
    
    void startGame() {
		System.out.println("Start Game");

        dealerPos = 0;
        // Start the game.
        table.run();
    }
    
    /**
     * The application's entry point.
     * 
     * @param args
     *            The command line arguments.
     */

    @Override
    public void joinedTable(Player playerToNotify, TableType type, BigDecimal bigBlind, List<Player> players) {
    	for (Player player : players) {
    		playerUpdated(playerToNotify, player);
    	}
    }

    @Override
    public void messageReceived(Player playerToNotify, String message) {
//        boardPanel.setMessage(message);
//        boardPanel.waitForUserInput();
    	sendMessage(playerToNotify.getEmail(), message);
    }
    
    public void sendMessage(String _user, String message) {
    	if(getParentRoom().getUserByName(_user) == null)
    		return;
        ISFSObject resObj = new SFSObject();
        resObj.putUtfString("text", message);
        send("setText", resObj, getParentRoom().getUserByName(_user));
    }

    public void sendMessage(String message) {
    	if(getParentRoom().getUserList() == null)
    		return;
        ISFSObject resObj = new SFSObject();
        resObj.putUtfString("text", message);
        send("setText", resObj, getParentRoom().getUserList());
    }

    @Override
    public void handStarted(Player playerToNotify, Player dealer) {
        setDealer(playerToNotify, dealerPos, false);
        dealerPos = dealer.getPos();
        dealerName = dealer.getName();
        setDealer(playerToNotify, dealer.getPos(), true);
    }

    public void setBlind(int _pos, String blindText)
    {
    	if(getParentRoom().getUserList() == null)
    		return;
        ISFSObject resObj = new SFSObject();
        resObj.putInt("pos", _pos);
        resObj.putUtfString("blindText", blindText);
        send("setBlind", resObj, getParentRoom().getUserList());
    }

    public void setBlind(Player blind, String blindText)
    {
    	if(getParentRoom().getUserList() == null)
    		return;
        ISFSObject resObj = new SFSObject();
        resObj.putInt("pos", blind.getPos());
        resObj.putUtfString("blindText", blindText);
        send("setBlind", resObj, getParentRoom().getUserList());
    }

    @Override
    public void setBlind(Player playerToNotify, Player blind, String blindText)
    {
    	if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
    		return;
        ISFSObject resObj = new SFSObject();
        resObj.putInt("pos", blind.getPos());
        resObj.putUtfString("blindText", blindText);
        send("setBlind", resObj, getParentRoom().getUserByName(playerToNotify.getEmail()));
    }
    
    @Override
    public void actorRotated(Player playerToNotify, Player actor) {
        setActorInTurn(playerToNotify, actorPos, false);
        actorPos = actor.getPos();
        actorName = actor.getName();
        setActorInTurn(playerToNotify, actorPos, true);
    }

    public void selectActor(Player actor, boolean show)
    {
    	if(getParentRoom().getUserList() == null)
    		return;
    	actorPos = actor.getPos();
    	ISFSObject resObj = new SFSObject();
    	resObj.putInt("actorPos", actorPos);
    	resObj.putBool("isShow", show);
    	resObj.putInt("waitingTime", actor.isBot() ? BOT_WAITING_TIME : ((speed) ? 10 : 20));
		send("setActor", resObj, getParentRoom().getUserList());
    }

    @Override
    public void selectActor(Player playerToNotify, Player actor, boolean show)
    {
    	if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
    		return;
    	actorPos = actor.getPos();
    	ISFSObject resObj = new SFSObject();
    	resObj.putInt("actorPos", actorPos);
    	resObj.putBool("isShow", show);
    	resObj.putInt("waitingTime", actor.isBot() ? BOT_WAITING_TIME : ((speed) ? 10 : 20));
		send("setActor", resObj, getParentRoom().getUserByName(playerToNotify.getEmail()));
    }
    
    public void updateBoard() {
    	if(getParentRoom().getUserList() == null)
    		return;
        ISFSObject resObj = new SFSObject();
        List<Integer> cardArray = new ArrayList<Integer>();
        for (Card card : table.board) {
        	cardArray.add(card.hashCode());
        }
        resObj.putIntArray("cards", cardArray);
        resObj.putInt("pot", table.getTotalPot().intValue());
        
        send("updateBoard", resObj, getParentRoom().getUserList());
    }

    @Override
    public void boardUpdated(Player playerToNotify, List<Card> cards, BigDecimal bet, BigDecimal pot) {
    	if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
    		return;
        playerUpdated(playerToNotify, playerToNotify);

        ISFSObject resObj = new SFSObject();
        List<Integer> cardArray = new ArrayList<Integer>();
        for (Card card : cards) {
        	cardArray.add(card.hashCode());
        }
        resObj.putIntArray("cards", cardArray);
        resObj.putInt("pot", pot.intValue());
        
        send("updateBoard", resObj, getParentRoom().getUserByName(playerToNotify.getEmail()));
    }

    @Override
    public void playerUpdated(Player playerToNotify, Player player) {
    	updatePlayer(playerToNotify, player, false);
    }
        
    public void updatePlayers(boolean isShow)
    {
    	prevShow = isShow;
        for (Player player : table.players) {
            if (!isShow) {
                // Hide secret information to other players.
                updatePlayer(player.publicClone(), false);
            }
            else
            	updatePlayer(player, false);
            if(!player.isBot())
            	updatePlayer(player, player, false);
        }
        for (Player player : table.newPlayers) {
        	updatePlayer(player, true);
        }
    }
    
    void updatePlayer(Player player, boolean isNew)
    {
    	if(getParentRoom().getUserList() == null)
    		return;
        ISFSObject resObj = new SFSObject();
        resObj.putInt("pos", player.getPos());
        resObj.putUtfString("email", player.getEmail());
        resObj.putUtfString("name", player.getName());
        resObj.putInt("avatar", player.getAvatar());
        resObj.putInt("cash", player.getCash().intValue());
        resObj.putInt("bet", player.getBet().intValue());
        resObj.putBool("isNew", isNew);
        Action action = player.getAction();
        if (action != null) {
            resObj.putUtfString("action", action.getName());
        } else {
            resObj.putUtfString("action", "");
        }
        if (player.hasCards()) {
            Card[] cards = player.getCards();
            if (cards.length == 2) {
                // Visible cards.
                resObj.putInt("card1", cards[0].hashCode());
                resObj.putInt("card2", cards[1].hashCode());
            } else {
                // Hidden cards (face-down).
                resObj.putInt("card1", CARD_BACK_NUM);
                resObj.putInt("card2", CARD_BACK_NUM);
            }
        } else {
            // No cards.
            resObj.putInt("card1", CARD_PLACEHOLDER_NUM);
            resObj.putInt("card2", CARD_PLACEHOLDER_NUM);
        }

        send("updatePlayer", resObj, getParentRoom().getUserList());
    }

    void updatePlayer(Player playerToNotify, Player player, boolean isNew)
    {
    	if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
    		return;
        ISFSObject resObj = new SFSObject();
        resObj.putInt("pos", player.getPos());
        resObj.putUtfString("email", player.getEmail());
        resObj.putUtfString("name", player.getName());
        resObj.putInt("avatar", player.getAvatar());
        resObj.putInt("cash", player.getCash().intValue());
        resObj.putInt("bet", player.getBet().intValue());
        resObj.putBool("isNew", isNew);
        Action action = player.getAction();
        if (action != null) {
            resObj.putUtfString("action", action.getName());
        } else {
            resObj.putUtfString("action", "");
        }
        if (player.hasCards()) {
            Card[] cards = player.getCards();
            if (cards.length == 2) {
                // Visible cards.
                resObj.putInt("card1", cards[0].hashCode());
                resObj.putInt("card2", cards[1].hashCode());
            } else {
                // Hidden cards (face-down).
                resObj.putInt("card1", CARD_BACK_NUM);
                resObj.putInt("card2", CARD_BACK_NUM);
            }
        } else {
            // No cards.
            resObj.putInt("card1", CARD_PLACEHOLDER_NUM);
            resObj.putInt("card2", CARD_PLACEHOLDER_NUM);
        }

        send("updatePlayer", resObj, getParentRoom().getUserByName(playerToNotify.getEmail()));
    }

    @Override
    public void playerActed(Player playerToNotify, Player player) {
    	Action action = player.getAction();
    	System.out.println(action.getName());
    	if (action != null) {
    		if(player.getClient() != this) {
    			waitForUserInput();
    		}
    	}
    }

    @Override
    public Action act(Player playerToNotify, BigDecimal minBet, BigDecimal currentBet, Set<Action> allowedActions) {
    	return getUserInput(playerToNotify, minBet, playerToNotify.getCash(), allowedActions);
    }

    /**
     * Sets whether the actor  is in turn.
     * 
     * @param isInTurn
     *            Whether the actor is in turn.
     */
    private void setActorInTurn(Player playerToNotify, int _pos, boolean isInTurn) {
    	if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
    		return;
    	ISFSObject resObj = new SFSObject();
    	resObj.putInt("actorPos", _pos);
    	resObj.putBool("isShow", isInTurn);
    	resObj.putInt("waitingTime", speed ? 10 : 20);
		send("setActor", resObj, getParentRoom().getUserByName(playerToNotify.getEmail()));
    }

    /**
     * Sets the dealer.
     * 
     * @param isDealer
     *            Whether the player is the dealer.
     */
    private void setDealer(Player playerToNotify, int _pos, boolean isDealer) {
    	if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
    		return;
    	ISFSObject resObj = new SFSObject();
    	resObj.putInt("dealerPos", _pos);
    	resObj.putBool("isDealer", isDealer);
		send("setDealer", resObj, getParentRoom().getUserByName(playerToNotify.getEmail()));
    }
    
	Room getGameRoom()
	{
		return this.getParentRoom();
	}
	
	void setSelectedAction(int mode, int value)
	{
		if(mode == 1)
			selectedAction = Action.FOLD;
		else if(mode == 2)
			selectedAction = Action.CHECK;
		else if(mode == 3)
			selectedAction = Action.CALL;
		else if(mode == 4)
			selectedAction = new RaiseAction(new BigDecimal(value));
		else if(mode == 5)
			selectedAction = new BetAction(new BigDecimal(value));
//		else if(str == "All-in")
//			selectedAction = Action.ALL_IN;
		System.out.println(selectedAction.getName() + mode);
		timer.cancel();
		synchronized (monitor) {
			monitor.notifyAll();
		}
	}
	
	
    /**
     * Waits for the user to click the Continue button.
     */
    public void waitForUserInput() {
    	selectedAction = null;
		// SetTimer
    	timer = new Timer();
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
			    // Your database code here
				  if(selectedAction == null) {
					  selectedAction = Action.CONTINUE;
					  synchronized (monitor) {
						  monitor.notifyAll();
					  }
				  }
			  }
			}, 1*1000);
		
		while(selectedAction == null) {
            // Wait for the user to select an action.
            synchronized (monitor) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    // Ignore.
                }
            }
		}
        timer.cancel();
    }
    
    /**
     * Waits for the user to click an action button and returns the selected
     * action.
     * 
     * @param minBet
     *            The minimum bet.
     * @param cash
     *            The player's remaining cash.
     * @param allowedActions
     *            The allowed actions.
     * 
     * @return The selected action.
     */
    public Action getUserInput(Player playerToNotify, BigDecimal minBet, BigDecimal cash, final Set<Action> allowedActions) {
		System.out.println("GetUserInput");
		selectedAction = null;
		
		if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
		{
			return null;
		}
        
        // Send client actions
       	ISFSObject resObj = new SFSObject();
       	resObj.putInt("minBet", minBet.intValue());
       	resObj.putInt("cash", cash.intValue());
       	resObj.putInt("actionSize", allowedActions.size());
       	int i = 0;
       	for(Action action : allowedActions) {
       		resObj.putUtfString("action" + i, action.getName());
       		i ++;
       	}
		send("getUserInput", resObj, getParentRoom().getUserByName(playerToNotify.getEmail()));
		
	    timer = new Timer();
		task = new TimerTask() {
			  @Override
			  public void run() {
			    // Your database code here
				  if(selectedAction == null) {
					  selectedAction = Action.FOLD;
					  synchronized (monitor) {
						  monitor.notifyAll();
					  }
				  }
			  }
			};
		timer.schedule(task, ((speed) ? 5 : 10) * 1000);
		
		while(selectedAction == null) {
            // Wait for the user to select an action.
            synchronized (monitor) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    // Ignore.
                }
            }
		}
        timer.cancel();
		if(getParentRoom().getUserByName(playerToNotify.getEmail()) == null)
		{
			return null;
		}
		send("hideButtons", new SFSObject(), getParentRoom().getUserByName(playerToNotify.getEmail()));
        
        return selectedAction;
    }
    
    public void kickPlayers()
    {
    	if(getParentRoom().getUserList() != null)
    		send("kick", new SFSObject(), getParentRoom().getUserList());
    	getApi().removeRoom(getParentRoom());
    }
    
    public void setPlayerChipcount(String email, int value)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "";
        if(table.playMode == PlayMode.CLUB_MODE)
        	sql = "UPDATE club_user SET club_chip=" + value + " WHERE email='" + email + "' AND club_id=" + table.club_id;
        else
        	sql = "UPDATE cash INNER JOIN users ON cash.id=users.id SET cash.chipcount=" + value + " WHERE users.email='" + email + "'";
        
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
    }
    
    public int getPlayerChipcount(String email)
    {
        int chipcount = 0;
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "";
        if(table.playMode == PlayMode.CLUB_MODE)
        	sql = "SELECT club_chip FROM club_user WHERE email='" + email + "' AND club_id=" + table.club_id;
        else
        	sql = "SELECT cash.chipcount FROM cash INNER JOIN users ON users.id=cash.id WHERE users.email='" + email + "'";
        
        System.out.println(sql + "," + table.club_id);
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            if(table.playMode == PlayMode.CLUB_MODE)
            	chipcount = res.getSFSObject(0).getInt("club_chip");
            else
            	chipcount = res.getSFSObject(0).getInt("chipcount");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return chipcount;
    }

    public String getUserName(String email)
	{
		String name = "";
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "SELECT username FROM users WHERE email='" + email +  "'";
        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            name = res.getSFSObject(0).getUtfString("username");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return name;
	}
    
    public ISFSObject getUser(String email)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "SELECT * FROM users WHERE email='" + email +  "'";
        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            return res.getSFSObject(0);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return null;
    }
    
    public ISFSObject getLocationInfo(String _location)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "SELECT * FROM location WHERE name='" + _location +  "'";
        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            return res.getSFSObject(0);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return null;
    }
    
    public void addHand(String _email)
    {
    	int hd = getHand(_email);
    	updateHand(_email, hd + 1);
    }
    
    public int getHand(String _email)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "SELECT hand FROM users WHERE email=\"" + _email +  "\"";        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            return res.getSFSObject(0).getInt("hand");
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return 0;
    }

    public void updateHand(String _email, int hd)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "UPDATE users SET hand=" + hd + " WHERE email=\"" + _email + "\"";
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
    }
    
    public void addHour(String _email, float _hr)
    {
    	float hr = getHour(_email);
    	updateHour(_email, hr + _hr);
    }
    
    public float getHour(String _email)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "SELECT hour FROM users WHERE email=\"" + _email +  "\"";        
        try
        {
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            return res.getSFSObject(0).getDouble("hour").floatValue();
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
        return 0;
    }

    public void updateHour(String _email, float _hr)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
        String sql = "UPDATE users SET hour=" + _hr + " WHERE email=\"" + _email + "\"";
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
    }
    
    
    public void addAction(String _email, int round, String _action, int amount)
    {
    	IDBManager dbManager = getParentZone().getDBManager();
    	String sql = "INSERT INTO hand_history(email, type, action, amount) VALUES("
    				+ "\"" + _email + "\","
    				+ round + ","
    				+ "\"" + _action + "\","
    				+ amount + ")";
        try
        {
            dbManager.executeUpdate(sql, new Object[] {});
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
    }
    
}
