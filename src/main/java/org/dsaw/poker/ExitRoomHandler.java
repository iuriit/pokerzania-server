package org.dsaw.poker;

import org.dsaw.poker.engine.Player;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ExitRoomHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		System.out.println("Ready");
		PokerExtension gameExt = (PokerExtension) getParentExtension();

		if (user.isPlayer())
		{
			gameExt.exitPlayer(user.getName());
		}
		
	}
}
