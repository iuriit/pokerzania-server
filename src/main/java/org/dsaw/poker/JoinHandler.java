package org.dsaw.poker;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class JoinHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		System.out.println("Join");
		PokerExtension gameExt = (PokerExtension) getParentExtension();

		if (user.isPlayer())
		{
			gameExt.joinNewPlayer(params.getInt("pos"), user.getName());
			gameExt.sendChipStatus(user.getName());
		}
		
	}
}
